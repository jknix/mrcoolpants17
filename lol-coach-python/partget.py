
def findtargetparticipant(target,participantIdentities):
    if type(participantIdentities) == list:
        t = {x['player']['summonerName']: x['participantId'] for x in participantIdentities}
        return t[target]
    else:
        return None


def findtargetparticipantchampid(target,participants):
    if type(participants) == list:
        t = {x['participantId']: x['championId'] for x in participants}
        return t[target]
    else:
        return None


def findtargetparticipantteam(target,participants):
    if type(participants) == list:
        t = {x['participantId']: x['teamId'] for x in participants}
        return t[target]
    else:
        return None

def findwin(target,participants):
    if type(participants) == list:
        t = {x['participantId']: x['stats']['win'] for x in participants}
        return t[target]
    else:
        return None

def findlane(target,participants):
    if type(participants) == list:
        t = {x['participantId']: x['timeline']['lane'] for x in participants}
        return t[target]
    else:
        return None

def findrole(target,participants):
    if type(participants) == list:
        t = {x['participantId']: x['timeline']['role'] for x in participants}
        return t[target]
    else:
        return None

def findcsdiffs(target,participants):
    if type(participants) == list:
        t = {x['participantId']: x['timeline']['csDiffPerMinDeltas'] for x in participants}
        return t[target]
    else:
        return None

def findkills(target,participants):
    if type(participants) == list:
        t = {x['participantId']: x['stats']['kills'] for x in participants}
        return t[target]
    else:
        return None

def finddeaths(target, participants):
    if type(participants) == list:
        t = {x['participantId']: x['stats']['deaths'] for x in participants}
        return t[target]
    else:
        return None

def findassists(target, participants):
    if type(participants) == list:
        t = {x['participantId']: x['stats']['assists'] for x in participants}
        return t[target]
    else:
        return None

def earlyxpgap(target, participants):
    if type(participants) == list:
        try:
            t = {x['participantId']: x['timeline']['xpDiffPerMinDeltas']['0-10'] for x in participants}
            # t = {x['participantId']: list(x['timeline'].keys()) for x in participants}
            return t[target] * 10
        except:
            return None
    else:
        return None
