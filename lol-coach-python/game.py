

def get_ExistingGames(accountid):
    from config import pgconnect

    conn = pgconnect()

    qry = 'select distinct gameid from gameparticipant where accountid = %s'
    cur = conn.cursor()
    cur.execute(qry, [accountid])
    games = cur.fetchall()
    cur.close()
    conn.close()

    return games


def get_APIGameList(accountid, count=200):
    from config import urls, settings
    from requests import request
    import time

    matches = []
    totalgames = request('GET', urls.base + urls.matchlist + str(accountid) + '?beginIndex=9999&endIndex=10000&api_key=' + settings.key).json()['totalGames']
    beginindex = totalgames - count
    print(totalgames, 'Total games. Retrieving last ' + str(min(count, totalgames)))
    while beginindex <= totalgames:
        print('Starting for game ', beginindex)
        indexstr = '?beginIndex=' + str(beginindex) + '&endIndex=' + str(beginindex + 99)
        newmatches = request('GET', urls.base + urls.matchlist + str(accountid) + indexstr + '&api_key=' + settings.key).json()['matches']
        matches += newmatches
        beginindex = beginindex + 100
        time.sleep(1.2)

    return matches

def insert_FullGame(gamedata):
    from config import pgBulkLoad, pgconnect
    from pandas import DataFrame
    import account
    from datetime import datetime

    starttime = datetime.now()

    intcols = ['gameid',
               'gamecreation',
               'gameduration',
               'queueid',
               'mapid',
               'seasonid',
               'winningteam']

    cols = ['gameid',
            'platformid',
            'gamecreation',
            'gameduration',
            'queueid',
            'mapid',
            'seasonid',
            'gameversion',
            'gamemode',
            'gametype',
            'winningteam']

    print('Got columns: ', datetime.now() - starttime)
    # Get winning team data and add to game frame
    gamedata.columns = [x.lower() for x in gamedata.columns]
    gameid = int(gamedata['gameid'][0])
    teamwins = {x['win']: x['teamId'] for x in gamedata['teams'][0]}['Win']
    gamedata['winningteam'] = teamwins

    print('Got winning team: ', datetime.now() - starttime)

    gamedata.reset_index(inplace=True)
    gameloaddata = gamedata.copy()
    gameloaddata = gameloaddata[cols]
    print('Got reset index and filtered: ', datetime.now() - starttime)

    for col in intcols:
        gameloaddata.loc[:, col] = gameloaddata[col].fillna(0).astype('int64')
    print('Made cols ints: ', datetime.now() - starttime)

    # Load game data
    load_gamedata_time = datetime.now() - starttime
    print('Loading game data: ', load_gamedata_time)
    pgBulkLoad(gameloaddata, 'game', cols)

    participantids = gamedata['participantidentities'][0]
    participantdf = DataFrame.from_records(participantids)
    participantdf.columns = [x.lower() for x in participantdf.columns]
    participantdf['accountid'] = [x['accountId'] for x in participantdf['player']]
    participantdf['id'] = [x['summonerId'] if 'summonerID' in x.keys() else None for x in participantdf['player']]
    participantdf['name'] = [x['summonerName'] for x in participantdf['player']]
    participantdf['summonerlevel'] = None
    participantdf['gameid'] = gameid

    participantdf_new = participantdf.copy()
    participantdf_new = participantdf_new[['accountid', 'name', 'summonerlevel', 'id']]
    participantdf_new = participantdf_new[participantdf_new['accountid'] != 0]
    newaccounts = account.exists_Accounts(tuple(participantdf_new['accountid']))['New']
    participantdf_new = participantdf_new[[x in newaccounts for x in participantdf_new['accountid']]]

    load_accountdata_time = datetime.now() - starttime
    print('Loading account data: ', load_accountdata_time)
    pgBulkLoad(participantdf_new, 'account', ['accountid', 'name', 'summonerlevel', 'id'])

    participantdata = gamedata['participants'][0]
    partdatadf = DataFrame.from_records(participantdata)
    partdatadf.columns = [x.lower() for x in partdatadf.columns]
    partdatadf['role'] = [x['role'] for x in partdatadf['timeline']]
    partdatadf['lane'] = [x['lane'] for x in partdatadf['timeline']]
    partdatadf_new = partdatadf.copy()
    partdatadf_new = partdatadf_new[['participantid',
                                     'championid',
                                     'highestachievedseasontier',
                                     'spell1id',
                                     'spell2id',
                                     'teamid',
                                     'role',
                                     'lane']]
    partdatadf_new.reset_index(inplace=True)

    partdatadf_new = partdatadf_new.merge(participantdf, on='participantid')

    partdatacols = ['gameid',
                    'participantid',
                    'accountid',
                    'championid',
                    'highestachievedseasontier',
                    'spell1id',
                    'spell2id',
                    'teamid',
                    'role',
                    'lane']

    load_partdata_time = datetime.now() - starttime
    print('Loading participant data: ', load_partdata_time)
    partdatadf_new = partdatadf_new[partdatacols]
    partdatadf_new = partdatadf_new[partdatadf_new['accountid'] != 0]
    pgBulkLoad(partdatadf_new, 'gameparticipant', partdatacols)

    gpidconn = pgconnect()
    qry = 'SELECT gameparticipantid, participantid from gameparticipant where Gameid = %s'
    cur = gpidconn.cursor()
    cur.execute(qry, (gameid,))
    dt = cur.fetchall()
    cur.close()
    gpidconn.close()
    dt = DataFrame.from_records(dt, columns=['gameparticipantid','participantid'])

    partdatadf = partdatadf.merge(dt, on='participantid')
    partdatadf_tl = partdatadf.copy()
    partdatadf_tl = partdatadf_tl[['gameparticipantid','timeline']]
    partdatadf_tl.reset_index(inplace=True)

    tlcols = ['creepsPerMinDeltas',
              'xpPerMinDeltas',
              'goldPerMinDeltas',
              'csDiffPerMinDeltas',
              'xpDiffPerMinDeltas',
              'damageTakenPerMinDeltas',
              'damageTakenDiffPerMinDeltas']

    tlstarts = {}
    for t in tlcols:
        partdatadf_tl.loc[:, t] = [x[t] if t in x.keys() else None
                                   for x
                                   in partdatadf_tl['timeline']]
        tlkeys = list(partdatadf_tl[t][0].keys()) if partdatadf_tl[t][0] is not None else {}
        tlstarts = {**tlstarts, **tlkeys}

    tldf = DataFrame.from_records([[x, y] for x in partdatadf_tl['gameparticipantid'] for y in tlstarts],
                                  columns=['gameparticipantid','timeframe'])
    tldf = tldf.merge(partdatadf_tl, on='gameparticipantid')
    tldf['timeintervalstart'] = [x.split('-')[0] for x in tldf['timeframe']]

    for t in tlcols:
        tldf[t] = [y[x] if y is not None else None
                   for x, y
                   in zip(tldf['timeframe'], tldf[t])]

    participant_tl_load_cols = ['gameparticipantid', 'timeintervalstart'] + tlcols
    tldf = tldf[participant_tl_load_cols]
    participant_tl_load_cols = [x.lower() for x in participant_tl_load_cols]
    tldf.columns = participant_tl_load_cols

    load_parttimelinedata_time = datetime.now() - starttime
    print('Loading participant tl data: ', load_parttimelinedata_time)
    pgBulkLoad(tldf,'gameparticipant_timeline',participant_tl_load_cols)

    runedf = partdatadf.copy()
    runedf = runedf[['gameparticipantid','runes']]
    runedataload = [[{**z, **{'gameparticipantid': y}} for z in x]
                    for x, y
                    in zip(runedf['runes'], runedf['gameparticipantid'])
                    if isinstance(x, list)]

    runedataload = DataFrame.from_records([record for part in runedataload for record in part])
    runedataload.columns = [x.lower() for x in runedataload.columns]
    runeloadcols = ['gameparticipantid', 'runeid', 'rank']
    runedataload = runedataload[runeloadcols]

    load_partrunedata_time = datetime.now() - starttime
    print('Loading participant rune data: ', load_partrunedata_time)
    pgBulkLoad(runedataload,'gameparticipant_runes', runeloadcols)

    masterydf = partdatadf.copy()
    masterydf = masterydf[['gameparticipantid', 'masteries']]
    mastdataload = [[{**z, **{'gameparticipantid': y}} for z in x]
                    for x, y
                    in zip(masterydf['masteries'], masterydf['gameparticipantid'])
                    if isinstance(x, list)]

    mastdataload = DataFrame.from_records([record for part in mastdataload for record in part])
    mastdataload.columns = [x.lower() for x in mastdataload.columns]
    mastloadcols = ['gameparticipantid', 'masteryid', 'rank']
    mastdataload = mastdataload[mastloadcols]

    load_partmastdata_time = datetime.now() - starttime
    print('Loading participant mastery data: ', load_partmastdata_time)
    pgBulkLoad(mastdataload, 'gameparticipant_masteries', mastloadcols)
