

def exists_Accounts(accountids):
    from config import pgconnect

    conn = pgconnect()

    qry = 'select distinct accountid from account where accountid IN %s'
    cur = conn.cursor()
    cur.execute(qry, (accountids,))
    dt = cur.fetchall()
    cur.close()
    conn.close()
    dt = [x[0] for x in dt]
    return {'Exists': [x for x in accountids if x in dt],
            'New': [x for x in accountids if x not in dt]}


def insert_NewAccount(accountdata):
    from config import pgconnect

    accountid = accountdata['accountId']
    id = accountdata['id']
    summonerlevel = accountdata['summonerLevel']
    name = accountdata['name']

    conn = pgconnect()
    ins_to = 'INSERT INTO account (accountid, name, summonerlevel, id)' \
             + ' VALUES (%s,%s,%s,%s)'
    cur = conn.cursor()
    cur.execute(ins_to, [accountid, name, summonerlevel, id])
    conn.commit()
    cur.close()
    conn.close()

    return True


def update_Account(accountdata):
    from config import pgconnect

    accountid = accountdata['accountId']
    id = accountdata['id']
    summonerlevel = accountdata['summonerLevel']
    name = accountdata['name']

    conn = pgconnect()
    upd_to = 'UPDATE account' \
             + ' SET id = %s' \
             + ' , name = %s' \
             + ' , summonerlevel = %s' \
             + ' where accountid = %s'
    cur = conn.cursor()
    cur.execute(upd_to, [id, name, summonerlevel, accountid])
    conn.commit()
    cur.close()
    conn.close()


class accountobj(object):

    def __init__(self, summonername):
        from config import urls
        from config import settings
        from requests import request
        import game

        info = request('GET', urls.base + urls.summonername + summonername + '?api_key=' + settings.key).json()
        self.settings = settings
        self.accountId = info['accountId']
        acctcheck = exists_Accounts([self.accountId])
        exists = self.accountId in acctcheck['Exists']

        if exists:
            upd = update_Account(accountdata=info)
        else:
            upd = insert_NewAccount(accountdata=info)

        self.existinggames = game.get_ExistingGames(self.accountId)

