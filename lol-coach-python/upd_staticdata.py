

def get_ExistStaticIds(qry):
    from config import pgconnect

    conn = pgconnect()
    cur = conn.cursor()
    cur.execute(qry)
    existids = cur.fetchall()
    existids = [x[0] for x in existids]
    cur.close()
    conn.close()

    return existids


def insert_Champion(champid, name):
    from config import pgconnect

    conn = pgconnect()
    ins_to = 'INSERT INTO lkp_champion (championid, championname)' \
             + ' VALUES (%s,%s)'
    cur = conn.cursor()
    cur.execute(ins_to, [champid, name])
    conn.commit()
    cur.close()
    conn.close()


def insert_Map(mapid, name):
    from config import pgconnect

    conn = pgconnect()
    ins_to = 'INSERT INTO lkp_map (mapid, mapname)' \
             + ' VALUES (%s,%s)'
    cur = conn.cursor()
    cur.execute(ins_to, [mapid, name])
    conn.commit()
    cur.close()
    conn.close()


def insert_Queue(queueid, queuename):
    from config import pgconnect

    conn = pgconnect()
    ins_to = 'INSERT INTO lkp_queue (queueid, queuename)' \
             + ' VALUES (%s,%s)'
    cur = conn.cursor()
    cur.execute(ins_to, [queueid, queuename])
    conn.commit()
    cur.close()
    conn.close()


def insert_Rune(runeid, runename, description):
    from config import pgconnect

    conn = pgconnect()
    ins_to = 'INSERT INTO lkp_runes (runeid, runename, description)' \
             + ' VALUES (%s,%s,%s)'
    cur = conn.cursor()
    cur.execute(ins_to, [runeid, runename, description])
    conn.commit()
    cur.close()
    conn.close()


def insert_Mastery(masteryid, masteryname, description):
    from config import pgconnect

    conn = pgconnect()
    ins_to = 'INSERT INTO lkp_mastery (masteryid, masteryname, description)' \
             + ' VALUES (%s,%s,%s)'
    cur = conn.cursor()
    cur.execute(ins_to, [masteryid, masteryname, description])
    conn.commit()
    cur.close()
    conn.close()


def insert_Item(itemid, itemname):
    from config import pgconnect

    conn = pgconnect()
    ins_to = 'INSERT INTO lkp_item (itemid, itemname)' \
             + ' VALUES (%s,%s)'
    cur = conn.cursor()
    cur.execute(ins_to, [itemid, itemname])
    conn.commit()
    cur.close()
    conn.close()


def insert_Item(itemid, itemname):
    from config import pgconnect

    conn = pgconnect()
    ins_to = 'INSERT INTO lkp_item (itemid, itemname)' \
             + ' VALUES (%s,%s)'
    cur = conn.cursor()
    cur.execute(ins_to, [itemid, itemname])
    conn.commit()
    cur.close()
    conn.close()


def insert_Spell(spellid, spellname, spellkey, sumlevel):
    from config import pgconnect

    conn = pgconnect()
    ins_to = 'INSERT INTO lkp_Spell (spellid, spellname, spellkey, summonerlevel)' \
             + ' VALUES (%s,%s,%s,%s)'
    cur = conn.cursor()
    cur.execute(ins_to, [spellid, spellname, spellkey, sumlevel])
    conn.commit()
    cur.close()
    conn.close()


def update_Champion():
    import requests as req
    from pandas import DataFrame
    from config import settings, urls

    champdata = req.request('GET', urls.base + urls.champs + '?api_key=' + settings.key).json()
    champdf = DataFrame.from_dict(champdata['data'], orient='index')
    champdf = champdf[['id', 'name']]

    existids = get_ExistStaticIds('select distinct championid from lkp_champion')

    notinlist = [x not in existids for x in champdf['id']]
    newchamps = champdf[notinlist]

    [insert_Champion(x, y)
     for x, y
     in zip(newchamps['id'], newchamps['name'])]

    return True


def update_Map():
    import requests as req
    from pandas import DataFrame
    from config import settings, urls

    mapdata = req.request('GET', urls.base + urls.maps + '?api_key=' + settings.key).json()
    mapdf = DataFrame.from_dict(mapdata['data'], orient='index')
    mapdf = mapdf[['mapId', 'mapName']]

    existids = get_ExistStaticIds('select distinct mapid from lkp_map')

    notinlist = [x not in existids for x in mapdf['mapId']]
    newmaps = mapdf[notinlist]

    [insert_Map(x, y)
     for x, y
     in zip(newmaps['mapId'], newmaps['mapName'])]

    return True


def update_Rune():
    import requests as req
    from pandas import DataFrame
    from config import settings, urls

    runedata = req.request('GET', urls.base + urls.rune + '?api_key=' + settings.key).json()
    runedf = DataFrame.from_dict(runedata['data'], orient='index')
    runedf = runedf[['id', 'name', 'description']]

    existids = get_ExistStaticIds('select distinct runeid from lkp_runes;')

    notinlist = [x not in existids for x in runedf['id']]
    newrunes = runedf[notinlist]

    [insert_Rune(x, y, z)
     for x, y, z
     in zip(newrunes['id'], newrunes['name'], newrunes['description'])]

    return True


def update_Item():
    import requests as req
    from pandas import DataFrame
    from config import settings, urls

    itemdata = req.request('GET', urls.base + urls.item + '?api_key=' + settings.key).json()
    itemdf = DataFrame.from_dict(itemdata['data'], orient='index')
    itemdf = itemdf[['id', 'name']]

    existids = get_ExistStaticIds('select distinct itemid from lkp_item;')

    notinlist = [x not in existids for x in itemdf['id']]
    newitems = itemdf[notinlist]

    [insert_Item(x, y)
     for x, y
     in zip(newitems['id'], newitems['name'])]

    return True


def update_Mastery():
    import requests as req
    from pandas import DataFrame, concat
    from config import settings, urls
    import time

    versions = req.request('GET', urls.base + urls.versions + '?api_key=' + settings.key).json()
    versions = [x for x in versions if int(x[0:1]) >= 4]

    mastdf = DataFrame()
    for v in versions:
        masterydata = req.request('GET', urls.base + urls.mastery + '?version' + v + '&api_key=' + settings.key).json()
        time.sleep(1.2)

        mastdf_t = DataFrame.from_dict(masterydata['data'], orient='index')
        mastdf_t = mastdf_t[['id', 'name', 'description']]
        mastdf = concat([mastdf, mastdf_t])

    mastdf.drop_duplicates(inplace=True)

    existids = get_ExistStaticIds('select distinct masteryid from lkp_mastery;')

    notinlist = [x not in existids for x in mastdf['id']]
    newmast = mastdf[notinlist]

    [insert_Mastery(x, y, z)
     for x, y, z
     in zip(newmast['id'], newmast['name'], newmast['description'])]

    return True


def update_Spell():
    import requests as req
    from pandas import DataFrame
    from config import settings, urls

    spelldata = req.request('GET', urls.base + urls.sumspell + '?api_key=' + settings.key).json()
    spelldf = DataFrame.from_dict(spelldata['data'], orient='index')
    spelldf = spelldf[['id', 'name', 'key', 'summonerLevel']]

    existids = get_ExistStaticIds('select distinct spellid from lkp_spell;')

    notinlist = [x not in existids for x in spelldf['id']]
    newspell = spelldf[notinlist]

    [insert_Spell(x, y, z, a)
     for x, y, z, a
     in zip(newspell['id'], newspell['name'], newspell['key'], newspell['summonerLevel'])]

    return True


# def update_Queue():
#     import requests as req
#     from pandas import DataFrame
#     from config import settings, urls
#
#     spelldata = req.request('GET', urls.base + urls.sumspell + '?api_key=' + settings.key).json()
#     spelldf = DataFrame.from_dict(spelldata['data'], orient='index')
#     spelldf = spelldf[['id', 'name', 'key', 'summonerLevel']]
#
#     existids = get_ExistStaticIds('select distinct spellid from lkp_spell;')
#
#     notinlist = [x not in existids for x in spelldf['id']]
#     newspell = spelldf[notinlist]
#
#     [insert_Spell(x, y, z, a)
#      for x, y, z, a
#      in zip(newspell['id'], newspell['name'], newspell['key'], newspell['summonerLevel'])]
#
#     return True

update_Champion()
update_Map()
update_Rune()
update_Item()
update_Mastery()
update_Spell()
# update_Queue()
