import requests as req
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import export_graphviz
import scipy as sp
import numpy as np
import time
import json
import matplotlib
import six
import zipfile as zf
import partget as partget
import tlget as tlget
import config as cf

urls = cf.urls
settings = cf.settings


class champs(object):
    data = req.request('GET',urls.base + urls.champs + '?api_key=' + settings.key)


class summoner(object):
    def __init__(self,summonername, settings):
        info = req.request('GET', urls.base + urls.summonername + summonername + '?api_key=' + settings.key).json()
        self.settings = settings
        self.accountId = info['accountId']
        self.id = info ['id']
        matches = []
        beginindex = 0
        totalgames = req.request('GET', urls.base+urls.matchlist+str(self.accountId)+'?beginIndex=9999&endIndex=10000&api_key='+settings.key).json()['totalGames']
        print(totalgames, 'games to retrieve')
        while beginindex <= totalgames:
            print('Starting for game ', beginindex)
            indexstr = '?beginIndex=' + str(beginindex) + '&endIndex=' + str(beginindex + 99)
            newmatches = req.request('GET', urls.base + urls.matchlist + str(self.accountId) + indexstr + '&api_key=' + settings.key).json()['matches']
            matches += newmatches
            beginindex = beginindex+100
            time.sleep(1.2)

        self.matchlist = matches

    def getallmatchdata(self, existingdata={}):
        self.matchids = [x['gameId'] for x in self.matchlist]

        matchdata = {}
        for match in self.matchids:
            if str(match) in list(existingdata.keys()):
                print('Adding existing match ', match)
                matchdata[str(match)] = existingdata[str(match)]
            else:
                print('Getting match ', match)
                time.sleep(1.2)
                matchdata[str(match)] = req.request('GET',urls.base+urls.match+str(match)+'?api_key='+self.settings.key).json()

        self.matchdata = matchdata


def getmatchtimeline(matchid, settings):
        print('Getting match ', matchid)
        time.sleep(1.2)
        tl = req.request('GET', urls.base+urls.matchtime+str(matchid)+'?api_key='+settings.key).json()
        return tl


with open('../backupdata/matches.json', 'r') as fp:
    backupdata = json.load(fp)

with open('../backupdata/timelines.json', 'r') as fp:
    backuptimeline = json.load(fp)


targetsummoner = 'mrcoolpants17'
mrcoolpants17 = summoner(targetsummoner, settings)
mrcoolpants17.getallmatchdata(existingdata=backupdata)

# Save cache of current matchdata
with open('backupdata/matches.json', 'w') as fp:
    json.dump(mrcoolpants17.matchdata, fp)

timelines = {str(x): backuptimeline[str(x)] if str(x) in list(backuptimeline.keys()) else getmatchtimeline(x, settings)
             for x
             in mrcoolpants17.matchids}

with open('backupdata/timelines.json', 'w') as fp:
    json.dump(timelines, fp)




# Format data in table
tabledata = pd.DataFrame.from_dict(mrcoolpants17.matchdata, orient='index')


# Remove 3v3 stuff
tabledata = tabledata[tabledata['queueId'] != 9]

tabledata['targetparticipant'] = [partget.findtargetparticipant(targetsummoner, x) for x in tabledata['participantIdentities']]
tabledata['targetteam'] = [partget.findtargetparticipantteam(y, x)
                           for x, y
                           in zip(tabledata['participants'],
                                  tabledata['targetparticipant'])]

tabledata['label'] = [partget.findwin(y, x)
                      for x, y
                      in zip(tabledata['participants'],
                             tabledata['targetparticipant'])]

tabledata['champid'] = [partget.findtargetparticipantchampid(y, x)
                        for x, y
                        in zip(tabledata['participants'],
                               tabledata['targetparticipant'])]

champnames = req.request('GET', urls.base+urls.champinfo+'?api_key='+settings.key).json()
champtable = pd.DataFrame.from_dict(champnames['data'], orient='index')

tabledata = tabledata.merge(champtable,
                            left_on='champid',
                            right_on='id')

tabledata['role'] = [partget.findrole(y, x)
                     for x, y
                     in zip(tabledata['participants'],
                            tabledata['targetparticipant'])]

tabledata['lane'] = [partget.findlane(y, x)
                     for x, y
                     in zip(tabledata['participants'],
                            tabledata['targetparticipant'])]

tabledata['kills'] = [partget.findkills(y, x)
                      for x, y
                      in zip(tabledata['participants'],
                             tabledata['targetparticipant'])]

tabledata['deaths'] = [partget.finddeaths(y, x)
                       for x, y
                       in zip(tabledata['participants'],
                              tabledata['targetparticipant'])]

tabledata['assists'] = [partget.findassists(y, x)
                        for x, y
                        in zip(tabledata['participants'],
                               tabledata['targetparticipant'])]

tabledata['xpdiff_10'] = [partget.earlyxpgap(y, x)
                          for x, y
                          in zip(tabledata['participants'],
                                 tabledata['targetparticipant'])]

tabledata['xy_kills'] = [print(x,y)
                         for x, y
                         in zip(tabledata['targetparticipant'],
                                tabledata['fulltimeline'])]

topchamps = tabledata['name'].value_counts()

classcols = ['label',
             'lane',
             'role',
             'kills',
             'deaths',
             'assists',
             'gameDuration',
             'name',
             'xpdiff_10']

classdata = tabledata[classcols].reset_index()
classdata_factorized = classdata.apply(lambda x: pd.factorize(x)[0])
classdata_factorized['train'] = np.random.uniform(0, 1, len(classdata)) <= .5

train, test = classdata_factorized[classdata_factorized['train']==True], classdata_factorized[classdata_factorized['train']==False]
trainy = pd.factorize(train['label'])

clf = RandomForestClassifier()

features = ['lane', 'role', 'name', 'kills', 'deaths', 'assists', 'gameDuration', 'xpdiff_10']

clf.fit(train[features], train['label'])

clf.predict(test[features])

test = test.reset_index()
test['result'] = clf.predict(test[features])
test[['result', 'label']]

pd.crosstab(test['result'], test['label'])
list(zip(train[features], clf.feature_importances_))
export_graphviz(clf.estimators_[0],
                feature_names=features,
                filled=True,
                rounded=True,
                out_file='tree.dot')


# matches = mrcoolpants17.matchlist['matches']
# matchtable = pd.DataFrame.from_records(matches)
tabledata.to_csv('tabledata.csv', index=False)


sammatchdata = req.request('GET',urls.base+urls.match+str(samplematchid)+'?api_key='+settings.key).json()
sammatchtimedata = req.request('GET',urls.base+urls.matchtime+str(samplematchid)+'?api_key='+settings.key).json()

parids = sammatchdata['participantIdentities']
parts = sammatchdata['participants']
teams = sammatchdata['teams']

sammatchdata.keys()
