

class urls(object):
    base = 'https://na1.api.riotgames.com/lol/'
    summonername = 'summoner/v3/summoners/by-name/'
    champs = 'static-data/v3/champions'
    champinfo = 'static-data/v3/champions/'
    matchlist = 'match/v3/matchlists/by-account/'
    match = 'match/v3/matches/'
    item = 'static-data/v3/items/'
    sumspell = 'static-data/v3/summoner-spells/'
    matchtime = 'match/v3/timelines/by-match/'
    maps = 'static-data/v3/maps'
    mastery = 'static-data/v3/masteries'
    rune = 'static-data/v3/runes'
    versions = 'static-data/v3/versions'


class settings(object):
    uname = 'thesoundandthefruity'
    key = 'RGAPI-d468000f-a5ea-4746-8f62-650615d6a67f'
    mapmin = {'x': -570, 'y': -420}
    mapmax = {'x': 15220, 'y': 14980}


class db(object):
    uname = 'postgre_admin'
    pw = 'postgre_admin'
    dbloc = 'mainpostgre.cekfqngoarnt.us-east-1.rds.amazonaws.com'
    dbport = 5432
    dbname = 'postgres'


def pgconnect():
    import psycopg2 as pgs
    conn = pgs.connect(host=db.dbloc,
                       dbname=db.dbname,
                       user=db.uname,
                       password=db.pw,
                       port=db.dbport)
    return conn


def pgBulkLoad(df, table_name, columns):
    import io

    output = io.StringIO()
    # ignore the index
    df.to_csv(output, sep='\t', header=False, index=False)
    output.getvalue()
    # jump to start of stream
    output.seek(0)

    connection = pgconnect()
    cursor = connection.cursor()
    # null values become ''
    cursor.copy_from(output, table_name, null="", columns=(columns))
    # cursor.copy_from(output, table_name, columns=columns)
    connection.commit()
    cursor.close()
