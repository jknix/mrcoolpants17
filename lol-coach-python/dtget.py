

def tl_xy_kill(target, timeline):
    if type(timeline) == dict:
        try:
            event = 'CHAMPION_KILL'
            timeline = [x['events'] for x in timeline['frames']]
            timeline = [e for l in timeline for e in l]
            timeline_targ = [x for x in timeline if x['type'] == event]
            timeline_targ_k = [x for x in timeline_targ if x['killerId'] == target]
            positions = {x['timestamp']/1000: x['position'] for x in timeline_targ_k}

            return positions

        except:
            return None
    else:
        return None

def tl_xy_death(target, timeline):
    if type(timeline) == dict:
        try:
            event = 'CHAMPION_KILL'
            timeline = [x['events'] for x in timeline['frames']]
            timeline = [e for l in timeline for e in l]
            timeline_targ = [x for x in timeline if x['type'] == event]
            timeline_targ_k = [x for x in timeline_targ if x['victimId'] == target]
            positions = {x['timestamp']/1000: x['position'] for x in timeline_targ_k}

            return positions

        except:
            return None
    else:
        return None

# todo add function to calculate early jungler pressure
# todo add function to calculate team ward presence for non ward players
# todo add function to calculate turret pressure
# todo add function to calculate damage effectiveness by tracking damage done as a pct of health that doesn't get kill
# todo add function to calculate support win rates that accounts for player win rates
# todo add function to calculate best kill vs death zones of the map
# todo add first to L6 function
# todo create calculation of game "pressure" as a combination of item builds, map control wards, etc; by frame/interval
# todo create/track leveling profile by item build/roles
# todo create track
# todo add function to calculate roam success by tracking kills out of lane prior to 20
