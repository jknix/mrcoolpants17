create table account
(
	accountid integer not null
		constraint account_pkey
			primary key,
	currentaccountid integer,
	currentplatformid integer,
	matchhistoryuri varchar(255),
	platformid integer,
	summonerid integer,
	summonername varchar(50)
)
;

create unique index account_accountid_uindex
	on account (accountid)
;

create table game
(
	gameid integer not null
		constraint game_pkey
			primary key,
	platformid integer,
	gamecreation integer,
	gameduration integer,
	queueid integer,
	mapid integer,
	seasonid integer,
	gameversion varchar(15),
	gamemode varchar(25),
	gametype varchar(25),
	winningteam integer
)
;

create unique index game_gameid_uindex
	on game (gameid)
;

create table gameparticipant
(
	gameparticipantid serial not null
		constraint gameparticipant_pkey
			primary key,
	gameid integer not null
		constraint gameparticipant_game_gameid_fk
			references game,
	participantid integer not null,
	accountid integer not null
		constraint gameparticipant_account_accountid_fk
			references account,
	championid integer not null,
	highestachievedseasontier varchar(50),
	spell1id integer,
	spell2id integer,
	teamid integer
)
;

create unique index gameparticipant_gameparticipantid_uindex
	on gameparticipant (gameparticipantid)
;

create table gameparticipant_masteries
(
	gameparticipant_masteriesid bigserial not null
		constraint gameparticipant_masteries_pkey
			primary key,
	gameparticipantid bigint not null
		constraint gameparticipant_masteries_gameparticipant_gameparticipantid_fk
			references gameparticipant,
	masteryid integer not null,
	rank integer not null
)
;

create unique index gameparticipant_masteries_gameparticipant_masteriesid_uindex
	on gameparticipant_masteries (gameparticipant_masteriesid)
;

create table lkp_mastery
(
	masteryid integer not null
		constraint lkp_mastery_pkey
			primary key,
	name varchar(255),
	description varchar(255)
)
;

create unique index lkp_mastery_masteryid_uindex
	on lkp_mastery (masteryid)
;

alter table gameparticipant_masteries
	add constraint gameparticipant_masteries_lkp_mastery_masteryid_fk
		foreign key (masteryid) references lkp_mastery
;

create table gameparticipant_timeline
(
	gameparticipant_timelineid bigserial not null
		constraint gameparticipant_timeline_pkey
			primary key,
	gameparticipantid bigint
		constraint gameparticipant_timeline_gameparticipant_gameparticipantid_fk
			references gameparticipant,
	timeintervalstart integer,
	"csDiffPerMinDeltas " numeric(13,4),
	"xpDiffPerMinDeltas " numeric(13,4),
	"creepsPerMinDeltas " numeric(13,4),
	goldpermindeltas numeric(13,4),
	"damageTakenDiffPerMinDeltas " numeric(13,4),
	"damageTakenPerMinDeltas " numeric(13,4),
	"xpPerMinDeltas " numeric(13,4)
)
;

create unique index gameparticipant_timeline_gameparticipant_timelineid_uindex
	on gameparticipant_timeline (gameparticipant_timelineid)
;

create table lkp_runes
(
	runeid integer not null
		constraint lkp_runes_pkey
			primary key,
	runename varchar(50),
	description varchar(255)
)
;

create unique index lkp_runes_runeid_uindex
	on lkp_runes (runeid)
;

create table lkp_champion
(
	championid integer not null
		constraint lkp_champion_pkey
			primary key,
	championname varchar(255)
)
;

create unique index lkp_champion_championid_uindex
	on lkp_champion (championid)
;

alter table gameparticipant
	add constraint gameparticipant_lkp_champion_championid_fk
		foreign key (championid) references lkp_champion
;

create table lkp_queue
(
	queueid integer,
	queuename varchar(255)
)
;

create table lkp_map
(
	mapid integer not null
		constraint lkp_map_pkey
			primary key,
	mapname varchar(255)
)
;

create unique index lkp_map_mapid_uindex
	on lkp_map (mapid)
;

create table gameparticipant_runes
(
	gameparticipant_runesid bigint not null
		constraint gameparticipant_runes_pkey
			primary key,
	gameparticipantid bigint
		constraint gameparticipant_runes_gameparticipant_gameparticipantid_fk
			references gameparticipant,
	runeid integer
		constraint gameparticipant_runes_lkp_runes_runeid_fk
			references lkp_runes,
	rank integer
)
;

create unique index gameparticipant_runes_gameparticipant_runesid_uindex
	on gameparticipant_runes (gameparticipant_runesid)
;

