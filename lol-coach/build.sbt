name := "lol-coach"

version := "0.1"

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % "10.0.10",
  "org.scalatest" %% "scalatest" % "3.0.1" % Test,
  "de.heikoseeberger" %% "akka-http-play-json" % "1.18.0"
)