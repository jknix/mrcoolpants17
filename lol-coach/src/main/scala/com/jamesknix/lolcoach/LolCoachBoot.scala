package com.jamesknix.lolcoach

import scala.io.StdIn
import scala.util.matching.Regex
import scala.util.{ Failure, Success }

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.{ Directives, Route }
import akka.stream.ActorMaterializer
import com.jamesknix.lolcoach.AnalysisService.{ AnalysisFailed, AnalysisSuccess }

/**
 * @author <a href="mailto:koleman@livesafemobile.com">Koleman Nix</a>
 */
object LolCoachBoot extends App with Directives {

  private final val summonerId: Regex = """.*""".r

  private final val analysisService: AnalysisService = new MockAnalysisService

  def routes: Route = {
    pathPrefix("analysis") {
      get {
        path(summonerId) { id =>
          logRequestResult("get_summoner_analysis", Logging.InfoLevel) {
            onComplete(analysisService.analyze(id)) {
              case Success(AnalysisSuccess(isGood)) => complete(isGood.toString)
              case Success(AnalysisFailed(maybeEx, msg)) => complete(msg)
              case Failure(ex) =>
                ex.printStackTrace()
                complete("bad times big error")
            }
          }
        }
      }
    }
  }

implicit val system = ActorSystem("lol-coach")
implicit val materializer = ActorMaterializer()
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext = system.dispatcher

  val bindingFuture = Http().bindAndHandle(routes, "localhost", 8080)

  println(s"LolCoach online at http://localhost:8080/\nPress RETURN to stop...")
  StdIn.readLine() // let it run until user presses return
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => system.terminate()) // and shutdown when done

}
