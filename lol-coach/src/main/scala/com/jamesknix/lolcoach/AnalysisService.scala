package com.jamesknix.lolcoach

import scala.concurrent.Future

import com.jamesknix.lolcoach.AnalysisService.{ AnalysisResult, AnalysisSuccess }

/**
 * @author <a href="mailto:koleman@livesafemobile.com">Koleman Nix</a>
 */
trait AnalysisService {

  def analyze(summonerName: String): Future[AnalysisResult]

}

object AnalysisService {

  sealed trait AnalysisResult
  case class AnalysisSuccess(goodPlayer: Boolean) extends AnalysisResult
  case class AnalysisFailed(ex: Option[Throwable], msg: String) extends AnalysisResult

}


class MockAnalysisService extends AnalysisService {
  override def analyze(summonerName: String): Future[AnalysisResult] = Future {
    val isGood: Boolean = summonerName == "mrcoolpants17"
    AnalysisSuccess(isGood)
  }(scala.concurrent.ExecutionContext.Implicits.global)
}
